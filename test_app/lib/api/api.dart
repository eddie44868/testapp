import 'dart:convert';
import 'package:http/http.dart' as http;

class CallApi {
  final String _url = 'http://202.157.184.201:8000/';

  postData(data, apiUrl) async {
    var fullUrl = _url + apiUrl;
    return await http.post(Uri.parse(fullUrl),
        body: jsonEncode(data), headers: _setHeaders());
  }

  getData(apiUrl) async {
    var fullUrl = _url + apiUrl;
    return await http.get(Uri.parse(fullUrl), headers: _setHeaders());
  }

  _setHeaders() => {
        'Content-type': 'Application/json',
        'Accept': 'Application/json',
      };
}
