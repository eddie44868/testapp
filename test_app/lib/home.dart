import 'package:flutter/material.dart';
import 'package:test_app/login.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: const Center(child: Text('Logged In Home')),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Image(
                image: AssetImage(
                  'assets/flutter.png',
                ),
                height: 200,
                width: 200,
              ),
              const SizedBox(
                height: 30,
              ),
              ElevatedButton(
                  style: raisedButtonStyle,
                  child: const Text(
                    "Logout",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 25,
                        fontWeight: FontWeight.bold),
                  ),
                  onPressed: () {
                    Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(builder: (c) => const LoginScreen()),
                        (route) => false);
                  }),
            ],
          ),
        ));
  }
}
