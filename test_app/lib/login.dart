import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:test_app/home.dart';
import 'package:test_app/register.dart';
import 'package:http/http.dart' as http;

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController mailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  bool _passwordVisible = true;
  _msg(msg) {
   Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0);
  }


  @override
  void initState() {
    super.initState();
    _passwordVisible = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: ListView(
            children: <Widget>[
              Container(
                alignment: Alignment.center,
                padding: const EdgeInsets.all(10),
                margin: const EdgeInsets.only(top: 40),
              ),
              const Padding(
                padding: EdgeInsets.all(8.0),
                child: Image(
                    height: 100,
                    width: 100,
                    image: NetworkImage(
                      'https://cdn.iconscout.com/icon/free/png-256/flutter-3629369-3032362.png',
                    )),
              ),
              const Text(
                'Login Page',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
              ),
              Container(
                padding: const EdgeInsets.all(10),
                child: TextField(
                  controller: mailController,
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(), labelText: "Email "),
                ),
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                child: TextField(
                  obscureText: !_passwordVisible,
                  controller: passwordController,
                  decoration: InputDecoration(
                    border: const OutlineInputBorder(),
                    labelText: "Password",
                    suffixIcon: IconButton(
                      icon: Icon(
                        _passwordVisible
                            ? Icons.visibility
                            : Icons.visibility_off,
                        color: Colors.black45,
                      ),
                      onPressed: () {
                        setState(() {
                          _passwordVisible = !_passwordVisible;
                        });
                      },
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              Container(
                height: 50,
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: ElevatedButton(
                    style: raisedButtonStyle,
                    child: const Text(
                      "Login",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    ),
                    onPressed: checkTextFieldEmptyOrNot),
              ),
              Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    const Text("   Does not have account?"),
                    TextButton(
                        // textColor: Colors.blue,
                        child: const Text(
                          "Register",
                          style: TextStyle(fontSize: 20),
                        ),
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const RegistScreen()));
                        })
                  ])
            ],
          ),
        ),
      ),
    );
  }


  checkTextFieldEmptyOrNot(){
    String text1,text2;
    text1 = mailController.text ;
    text2 = passwordController.text ;
 
    // Checking all TextFields.
    if(text1 == '' || text2 == '')
    {
      _msg('Form Tidak Boleh Kosong!');
 
    }else{
      _login();
    }
    
  }

  void _login() async {
    int respon;
    var data = {
      'username': mailController.text,
      'password': passwordController.text
    };
    var url = 'http://202.157.184.201:8000/login';
    http.post(Uri.parse(url), body: data).then((response) {
      respon = response.statusCode;
      print("Response status: ${response.statusCode}");
      print("Response body: ${response.body}");
      print(respon);
      var body = json.decode(response.body);
      if (body['status']['kode'] == 'success') {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => const HomeScreen()));
      } else {
        _msg(body['status']['keterangan'].toString());
      }
    });
  }
}

final ButtonStyle raisedButtonStyle = ElevatedButton.styleFrom(
  onPrimary: Colors.grey[300],
  primary: Colors.blue[300],
  minimumSize: const Size(88, 36),
  padding: const EdgeInsets.symmetric(horizontal: 16),
  shape: const RoundedRectangleBorder(
    borderRadius: BorderRadius.all(Radius.circular(6)),
  ),
);
