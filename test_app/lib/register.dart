import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:test_app/api/api.dart';
import 'package:test_app/home.dart';
import 'package:test_app/login.dart';

class RegistScreen extends StatefulWidget {
  const RegistScreen({Key? key}) : super(key: key);

  @override
  _RegistScreenState createState() => _RegistScreenState();
}

class _RegistScreenState extends State<RegistScreen> {
  final TextEditingController _FnameController = TextEditingController();
  final TextEditingController _LnameController = TextEditingController();
  final TextEditingController _phoneController = TextEditingController();
  final TextEditingController _mailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController ConpasswordController = TextEditingController();
  bool _passwordVisible = true;

  _msg(msg) {
    final toast = Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  @override
  void initState() {
    super.initState();
    _passwordVisible = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: ListView(
            children: <Widget>[
              const Padding(
                padding: EdgeInsets.all(8.0),
                child: Image(
                    height: 100,
                    width: 100,
                    image: NetworkImage(
                      'https://cdn.iconscout.com/icon/free/png-256/flutter-3629369-3032362.png',
                    )),
              ),
              const Text(
                'Register',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(10, 5, 10, 0),
                child: TextField(
                  controller: _FnameController,
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(), labelText: "Nama Depan "),
                ),
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(10, 5, 10, 0),
                child: TextField(
                  controller: _LnameController,
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(), labelText: "Nama Belakang"),
                ),
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(10, 5, 10, 0),
                child: TextField(
                  controller: _mailController,
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(), labelText: "Email "),
                ),
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(10, 5, 10, 0),
                child: TextField(
                  controller: _phoneController,
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(), labelText: "No.Hp "),
                ),
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(10, 5, 10, 0),
                child: TextField(
                  obscureText: !_passwordVisible,
                  controller: passwordController,
                  decoration: InputDecoration(
                    suffixIcon: IconButton(
                      icon: Icon(
                        _passwordVisible
                            ? Icons.visibility
                            : Icons.visibility_off,
                        color: Colors.black45,
                      ),
                      onPressed: () {
                        setState(() {
                          _passwordVisible = !_passwordVisible;
                        });
                      },
                    ),
                      border: const OutlineInputBorder(), labelText: "Password"),
                ),
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(10, 5, 10, 0),
                child: TextField(
                  obscureText: !_passwordVisible,
                  controller: ConpasswordController,
                  decoration: InputDecoration(
                    suffixIcon: IconButton(
                      icon: Icon(
                        _passwordVisible
                            ? Icons.visibility
                            : Icons.visibility_off,
                        color: Colors.black45,
                      ),
                      onPressed: () {
                        setState(() {
                          _passwordVisible = !_passwordVisible;
                        });
                      },
                    ),
                      border: const OutlineInputBorder(),
                      labelText: "Confirm Password"),
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              Container(
                height: 50,
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: ElevatedButton(
                    style: raisedButtonStyle,
                    child: const Text(
                      "Register",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    ),
                    onPressed: check),
              ),
              Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    const Text("   Already have account?"),
                    TextButton(
                        // textColor: Colors.blue,
                        child: const Text(
                          "Login",
                          style: TextStyle(fontSize: 20),
                        ),
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const LoginScreen()));
                        })
                  ])
            ],
          ),
        ),
      ),
    );
  }

  check(){
    String text1,text2,text3,text4,text5,text6;
    text1 = _mailController.text ;
    text2 = passwordController.text ;
    text3 = _FnameController.text ;
    text4 = _LnameController.text ;
    text5 = _phoneController.text ;
    text6 = ConpasswordController.text ;

    if(text1 == '' || text2 == '' || text3 == '' || text4 == '' || text5 == '' || text6 == '')
    {
      _msg('Form Tidak Boleh Kosong!');
 
    }else{
      if (text2 != text6) {
      _msg('Password Tidak Sesuai');
    } else {
      _handleRegis();
    }
    
  }
 }


  void _handleRegis() async {
    var data = {
      'email': _mailController.text,
      'hp': _phoneController.text,
      "firstname": _FnameController.text,
      "lastname": _LnameController.text,
      "grup": "member",
      "password": passwordController.text
    };
    
      var res = await CallApi().postData(data, 'users');
      var body = json.decode(res.body);
      print(body);
      if (body['status']['kode'] == 'success') {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => const HomeScreen()));
      } else {
        _msg(body['status']['keterangan'].toString());
      }
    }
  }

final ButtonStyle raisedButtonStyle = ElevatedButton.styleFrom(
  onPrimary: Colors.grey[300],
  primary: Colors.blue[300],
  minimumSize: const Size(88, 36),
  padding: const EdgeInsets.symmetric(horizontal: 16),
  shape: const RoundedRectangleBorder(
    borderRadius: BorderRadius.all(Radius.circular(6)),
  ),
);
